<?php
	/*
		Template Name: Management
	*/
	get_header();
?>
	<div class="uk-container uk-container-center page management">
		<div class="uk-grid" data-uk-grid-match>
			<div class="uk-width-medium-7-10">
			<h1><?php the_title(); ?></h1>
			<?php
				$args = array(
					"parent" => "216"
				);
				foreach(get_pages($args) as $post) : setup_postdata($post);
			?>
				<article class="uk-article archive">
					<h3 class="uk-article-title"><?php the_title(); ?></h3>
					<?php the_post_thumbnail("full", array("class"=>"uk-align-left")); ?>
					<?php the_content(); ?>
				</article>
			<?php endforeach; ?>
			</div>
			<div class="uk-width-3-10 uk-hidden-small">
				<?php get_sidebar("page"); ?>
			</div>
		</div>
	</div>
<?php
	get_footer();
