<?php
	get_header();
?>
<div class="uk-container uk-container-center archive partners">
	<div class="uk-grid" data-uk-grid-match>
		<div class="uk-width-medium-7-10">
			<h1>Partners</h1>
			<div class="sub">Take a look at some of the wonderful partners we have the fortune to work with.</div>
			<?php
				global $wp_query;
				$args = array(
					"orderby"	=> "title",
					"order"		=> "ASC"
				);
				$args = array_merge($wp_query->query_vars, $args);
				query_posts($args);
				while(have_posts()) : the_post();
			?>
				<article class="uk-article">
					<h3 class="uk-article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail("thumbnail", array("class"=>"uk-align-left")); ?></a>
					<?php the_excerpt(); ?>
				</article>
			<?php endwhile; ?>
			<ul class="uk-pagination">
				<li class="uk-pagination-previous"><?php previous_posts_link(); ?></li>
				<li class="uk-pagination-next"><?php next_posts_link(); ?></li>
			</ul>
		</div>
		<div class="uk-width-3-10 uk-hidden-small">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>


<?php
	get_footer();