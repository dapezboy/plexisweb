<?php
	get_header();
?>
	<div class="uk-container uk-container-center uk-text-center">
		<h1>Error 404: Not Found</h1>
		<div class="uk-text-large">Looks like you took a wrong turn! Lets take the easy path <a href="<?php echo home_url(); ?>">home</a>.</div>
	</div>
<?php
	get_footer();