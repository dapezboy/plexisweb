<?php
	get_header();
?>
	<div class="uk-container uk-container-center">
		<div class="uk-grid" data-uk-grid-match>
			<div class="uk-width-medium-7-10">
			<?php while(have_posts()) : the_post(); ?>
				<article class="uk-article post single">
					<h1 class="uk-article-title"><?php the_title(); ?></h1>
					<?php the_post_thumbnail("thumbnail", array("class"=>"uk-align-left")); ?>
					<?php the_content(); ?>
					<div class="uk-margin-top">
						<?php $link = get_post_meta($post->ID, "url", true); ?>
						Link: <a href="<?php echo $link ?>" target="_blank"><?php echo $link; ?></a>
					</div>
				</article>
			<?php endwhile; ?>
			</div>
			<div class="uk-width-3-10 uk-hidden-small">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php
	get_footer();
