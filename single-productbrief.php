<?php
	get_header();
?>
	<div class="uk-container uk-container-center">
		<div class="uk-grid" data-uk-grid-match>
			<div class="uk-width-medium-7-10">
			<?php while(have_posts()) : the_post(); ?>
				<article class="uk-article post single product-brief">
					<h1 class="uk-article-title"><?php the_title(); ?></h1>
					<?php the_content(); ?>
					<?php echo do_shortcode('[productbrief id="' . get_the_ID() . '"]'); ?>
				</article>
			<?php endwhile; ?>
			</div>
			<div class="uk-width-3-10 uk-hidden-small">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php
	get_footer();
