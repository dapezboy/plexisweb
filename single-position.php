<?php
	get_header();
?>
	<div class="uk-container uk-container-center">
		<div class="uk-grid" data-uk-grid-match>
			<div class="uk-width-medium-7-10">
			<?php while(have_posts()) : the_post(); ?>
				<article class="uk-article post single">
					<h1 class="uk-article-title"><?php the_title(); ?></h1>
					<div class="uk-margin-bottom">
					<?php $pdf = get_post_meta($post->ID, "pdf", true); ?>
						<a href="<?php echo $pdf['guid']; ?>"><i class="uk-icon uk-icon-file-pdf-o"> <?php echo $pdf['post_title']; ?>.pdf</i></a>
					</div>
					<?php the_content(); ?>
					<?php echo FrmFormsController::get_form_shortcode(array('id' => 8, 'title' => false, 'description' => false)); ?>
				</article>
			<?php endwhile; ?>
			</div>
			<div class="uk-width-3-10 uk-hidden-small">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php
	get_footer();
