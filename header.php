<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">

		<title><?php wp_title( '|', true, 'right' ); ?><?php echo get_bloginfo( "name", "raw"); ?></title>

		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

		<link href="<?php echo get_theme_part("css", "base.css"); ?>" rel="stylesheet" type="text/css" />
		<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>
		<link href="<?php echo get_stylesheet_uri(); ?>" rel="stylesheet" type="text/css" />

		<script src="//uikit/vendor/jquery.js"></script>
		<script src="<?php echo get_theme_part("js", "base.js"); ?>"></script>

		<?php if(is_front_page()) : ?>
			<script src="<?php echo get_theme_part("js", "bxslider.min.js"); ?>"></script>
			<link href="<?php echo get_theme_part("css", "bxslider.css"); ?>" rel="stylesheet" type="text/css" />
		<?php endif; ?>

		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<?php wp_head(); ?>
	</head>
	<body id="top" <?php body_class(); ?>>
		<?php
			$menuargs = array(
				"container"			=> "",
				"theme_location"	=> "primary_nav",
				"menu_class"		=> "uk-navbar-nav uk-float-right uk-visible-large",
				"menu_id"			=> "",
				"depth"				=> 2,
				"walker"			=> new Walker_UIKIT
			);
			$offcanvnav = array(
				"container"			=> "",
				"theme_location"	=> "primary_nav",
				"menu_class"		=> "uk-nav uk-nav-offcanvas",
				"menu_id"			=> ""
			);
		?>
		<header class="uk-container uk-container-center uk-margin-top uk-margin-large-bottom">
			<div class="uk-grid header">
				<div class="uk-width-medium-3-10 uk-width-9-10">
					<a href="<?php echo home_url(); ?>"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" alt="Plexis" /></a>
				</div>
				<div class="uk-width-medium-7-10 uk-width-1-10">
					<nav class="uk-nav">
						<?php wp_nav_menu($menuargs); ?>
						<a href="#off-canv" class="uk-float-right uk-hidden-large uk-navbar-toggle" data-uk-offcanvas></a>
					</nav>
					<div id="off-canv" class="uk-offcanvas">
						<div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
							<?php wp_nav_menu($offcanvnav); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="border"></div>
		</header>