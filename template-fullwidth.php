<?php
	/*
		Template Name: Full Width
	*/
	get_header();
?>
	<div class="uk-container uk-container-center">
		<div class="uk-width-1-1">
		<?php while(have_posts()) : the_post(); ?>
			<article class="uk-article product page">
				<h1 class="uk-article-title"><?php the_title(); ?></h1>
				<?php the_post_thumbnail("full", array("class"=>"uk-align-center")); ?>
				<?php the_content(); ?>
			</article>
		<?php endwhile; ?>
		</div>
	</div>
<?php
	get_footer();
