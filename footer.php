<footer class="uk-width-1-1 uk-margin-large-top">
	<div class="uk-container uk-container-center">
		<div class="uk-grid">
			<div class="uk-width-small-1-2 uk-width-medium-1-2 uk-width-large-1-4">
				<?php dynamic_sidebar('footer-1'); ?>
			</div>
			<div class="uk-width-small-1-2 uk-width-medium-1-2 uk-width-large-1-4">
				<?php dynamic_sidebar('footer-2'); ?>
			</div>
			<div class="uk-width-small-1-2 uk-width-medium-1-2 uk-width-large-1-4">
				<?php dynamic_sidebar('footer-3'); ?>
			</div>
			<div class="uk-width-small-1-2 uk-width-medium-1-2 uk-width-large-1-4">
				<?php dynamic_sidebar('footer-4'); ?>
			</div>
		</div>
		<div class="uk-width-1-1">
			<div class="uk-float-left">Copyright 2014.</div>
			<div class="uk-float-right">
				<a href="#top" data-uk-smooth-scroll="{offset: 90}"><i class="uk-icon uk-icon-arrow-up"> Back to Top</i></a>
			</div>
			<div class="uk-clearfix"></div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>