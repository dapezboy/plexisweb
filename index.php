<?php
	get_header();
?>
<div class="home uk-container uk-container-center">
	<section class="slider">
	<?php $the_query = new WP_Query(array("post_type" => "slide")); ?>
		<ul class="bxslider">
			<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
				<li>
					<a href="<?php echo get_post_meta($post->ID, "link", true); ?>">
						<?php echo get_the_post_thumbnail($post->ID, "slider-img"); ?>
					</a>
					<h2><?php the_title(); ?></h2>
					<div class="sub"><?php echo get_post_meta($post->ID, "sub_title", true); ?></div>
				</li>
			<?php endwhile; ?>
		</ul>
		<script>
			$(document).ready(function() {
				$('.bxslider').bxSlider({
					mode: 'fade',
					auto: true,
					autoHover: true,
					useCSS: true,
					adaptiveHeight: true,
					preloadImages: 'visible'
				});
			});
		</script>
		<?php wp_reset_postdata(); ?>
	</section>
	<section class="tricolumn">
		<div class="uk-grid">
		<?php
			$c1 = get_post_meta(206, "Column1");
			$c2 = get_post_meta(206, "Column2");
			$c3 = get_post_meta(206, "Column3");
		?>
			<div class="uk-width-medium-1-3">
				<div class="uk-panel">
					<div class="title">
						<img class="uk-align-left" src="<?php echo get_template_directory_uri(); ?>/images/logo_wings.png" alt="plexis wings" />
						<h3><?php echo $c1[0]; ?></h3>
					</div>
					<p><?php echo $c1[1]; ?></p>
				</div>
			</div>
			<div class="uk-width-medium-1-3">
				<div class="uk-panel">
					<div class="title">
						<img class="uk-align-left" src="<?php echo get_template_directory_uri(); ?>/images/logo_wings.png" alt="plexis wings" />
						<h3><?php echo $c2[0]; ?></h3>
					</div>
					<p><?php echo $c2[1]; ?></p>
				</div>
			</div>
			<div class="uk-width-medium-1-3">
				<div class="uk-panel">
					<div class="title">
						<img class="uk-align-left" src="<?php echo get_template_directory_uri(); ?>/images/logo_wings.png" alt="plexis wings" />
						<h3><?php echo $c3[0]; ?></h3>
					</div>
					<p><?php echo $c3[1]; ?></p>
				</div>
			</div>
		</div>
	</section>
	<section class="software">
		<div class="intro">
			<?php echo get_post_meta(206, "SoftwareBody", true); ?>
		</div>
		<div class="uk-grid">
		<?php
			$sc1 = get_post_meta(206, "SoftwareColumn1");
			$sc2 = get_post_meta(206, "SoftwareColumn2");
			$sc3 = get_post_meta(206, "SoftwareColumn3");
		?>
		<div class="uk-width-medium-1-3">
			<div class="uk-panel">
				<div class="uk-panel-teaser">
					<a href="<?php echo $sc1[3]; ?>"><img src="<?php echo wp_get_attachment_url($sc1[0]); ?>" alt="<?php echo $sc1[1]; ?>" /></a>
				</div>
				<div class="uk-panel-title">
					<a href="<?php echo $sc1[3]; ?>"><h3><?php echo $sc1[1]; ?></h3></a>
				</div>
				<?php echo $sc1[2]; ?>
			</div>
		</div>
		<div class="uk-width-medium-1-3">
			<div class="uk-panel">
				<div class="uk-panel-teaser">
					<a href="<?php echo $sc2[3]; ?>"><img src="<?php echo wp_get_attachment_url($sc2[0]); ?>" alt="<?php echo $sc2[1]; ?>" /></a>
				</div>
				<div class="uk-panel-title">
					<a href="<?php echo $sc2[3]; ?>"><h3><?php echo $sc2[1]; ?></h3></a>
				</div>
				<?php echo $sc2[2]; ?>
			</div>
		</div>
		<div class="uk-width-medium-1-3">
			<div class="uk-panel">
				<div class="uk-panel-teaser">
					<a href="<?php echo $sc3[3]; ?>"><img src="<?php echo wp_get_attachment_url($sc3[0]); ?>" alt="<?php echo $sc3[1]; ?>" /></a>
				</div>
				<div class="uk-panel-title">
					<a href="<?php echo $sc3[3]; ?>"><h3><?php echo $sc3[1]; ?></h3></a>
				</div>
				<?php echo $sc3[2]; ?>
			</div>
		</div>
		</div>
	</section>
</div>
<?php
	get_footer();