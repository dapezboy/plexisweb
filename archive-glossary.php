<?php
	get_header();
?>
<div class="uk-container uk-container-center archive glossary">
	<div class="uk-grid" data-uk-grid-match>
		<div class="uk-width-medium-7-10">
			<?php
				$alphas = range('A', 'Z');
				function alpha_list($current, $alphas) { ?>
					<div class="alpha-list">
						<?php foreach($alphas as $alpha) : ?>
							<a class="alpha-item <?php if($current === $alpha) echo "active"; ?>" href="./?l=<?php echo $alpha; ?>"><?php echo $alpha; ?></a>
						<?php endforeach; ?>
					</div>
				<?php } 
				$pod = pods("glossary");
				$letter = get_query_var("l");
				if(!empty($letter)) {
					$params = array(
						"limit" => -1,
						"where" => "t.post_title LIKE '$letter%'",
						"orderby" => "t.post_title"
					);
					$pod->find($params);
				}
			?>
			<?php if (0 < $pod->total()) : ?>
				<h1>Glossary beginning with <?php echo $letter; ?></h1>
				<?php alpha_list($letter, $alphas); ?>
				<dl class="uk-description-list-line">
				<?php while($pod->fetch()) : ?>
					<dt id="<?php echo $pod->id(); ?>"><!-- <a href="<?php echo $pod->display("permalink"); ?>"> if enabled may need to update css --><?php echo $pod->display("name"); ?><!-- </a> --></dt>
					<dd class="uk-margin-left"><?php echo $pod->display("excerpt"); ?></dd>
				<?php endwhile; ?>
				</dl>
			<?php elseif(0 == $pod->total() && $letter !== "") : ?>
				<h1>Glossary beginning with <?php echo $letter; ?></h1>
				<?php alpha_list($letter, $alphas); ?>
				<div class="uk-text-center uk-margin-top">No <i>terms</i> were found in our glossary for that letter.</div>
			<?php else : ?>
				<h1>Glossary</h1>
				<?php alpha_list($letter, $alphas); ?>
				<div class="uk-text-center uk-margin-top">Click one of the letters above to go to the page of all terms beginning with that letter.</div>
			<?php endif; ?>
		</div>
		<div class="uk-width-3-10 uk-hidden-small">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>


<?php
	get_footer();