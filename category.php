<?php
	get_header();
?>
<div class="uk-container uk-container-center category archive">
	<div class="uk-grid" data-uk-grid-match>
		<div class="uk-width-medium-7-10">
			<h1><?php single_cat_title(); ?></h1>
			<?php while(have_posts()) : the_post(); ?>
				<article class="uk-article">
					<h3 class="uk-article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<?php the_excerpt(); ?>
					<a class="uk-float-right" href="<?php the_permalink(); ?>">Read More <i class="uk-icon-caret-right"></i></a>
				</article>
			<?php endwhile; ?>
			<ul class="uk-pagination">
				<li class="uk-pagination-previous"><?php previous_posts_link(); ?></li>
				<li class="uk-pagination-next"><?php next_posts_link(); ?></li>
			</ul>
		</div>
		<div class="uk-width-3-10 uk-hidden-small">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>


<?php
	get_footer();